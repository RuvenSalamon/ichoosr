import minimist from "minimist";
import sqlite3 from "sqlite3";
import parse from "csv-parse";
import { createReadStream } from 'fs';
import express from "express";

import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const tableName = "cameras";
let db;

const query = (query) => {
    return new Promise((resolve, reject) => {
        db.all(query, (e, rows) => e ? reject(e) : resolve(rows));
    });
}

const getAll = () => {
    return query("select * from cameras");
}

const search = (name) => {
    return query(`select * from cameras where Camera like '%${name}%'`);
};

const extractNameId = (record) => {
    return record.Camera.split(" ")[0].split("-")[2];
};

const sortRecordsOnNameId = (records) => {
    return records.sort((first, second) => {
        const firstNameId = extractNameId(first);
        const secondNameId = extractNameId(second);
        return parseInt(firstNameId) > parseInt(secondNameId) ? 1 : -1;
    });
};

const printRecord = (record) => {
    const nameId = extractNameId(record);
    console.log(`${nameId} | ${record.Camera} | ${record.Latitude} | ${record.Longitude}`);
};

const runAsCli = async () => {
    // console.debug("running as cli");
    const parsedArguments = minimist(process.argv.slice(2), {string: "name"});

    if (!parsedArguments.name)  {
        console.error("Could not parse arguments");
        return;
    }

    // console.debug("searching for ", parsedArguments.name);
    
    const records = await search(parsedArguments.name);
    
    // console.debug("found: ", records);

    const sortedRecords = sortRecordsOnNameId(records);
    sortedRecords.forEach(printRecord);
};

const runAsServer = async () => {
    // console.debug("running as server");
    
    const app = express();
    const port = 3000;

    app.get("/", (req, res) => {
        res.sendFile(path.join(__dirname, '/index.html'));
    })

    app.get("/cameras", async (req, res) => {
        const queryParam = req.query.Camera;
        const records = await (queryParam ? search(queryParam) : getAll());
        res.send(records);
    })

    app.listen(port, () => {
        console.debug("listening on port ", port);
    })
};

const createDb = () => {
    const callback = (e) => e && console.error("Failed to initialize database: ", e);
    return new sqlite3.Database(":memory:", callback);
}

const insertRow = (db, record) => {
    return new Promise((resolve, reject) => {
        const insertQuery = `insert into cameras (Camera, Latitude, Longitude) values ('${record[0]}', ${record[1]}, ${record[2]});`
        // console.debug("inserting row: ", insertQuery);
        db.run(insertQuery, (e) => e ? reject(e) : resolve());
    });
};

const populateWithCsv = (db) => {
    let firstLine = true;
    const promises = [];
    
    const handleRecord = (record) => {        
        if (firstLine) {
            firstLine = false;
            return;
        }

        promises.push(insertRow(db, record));
    };

    promises.push(new Promise((resolve) => {
        const readStream = createReadStream("./cameras-defb.csv");
        const parser = parse({delimiter: ';', skipLinesWithError: true});
    
        parser.on('data', handleRecord);   
        parser.on('end', resolve);

        readStream.pipe(parser);
    }));

    return Promise.all(promises);
};

const createTable = (db) => {
    return new Promise((resolve, reject) => {
        const createTableQuery = "create table cameras (id integer primary key, Camera text not null, Latitude real not null, Longitude real not null);";
        db.exec(createTableQuery, (e) => e ? reject(e) : resolve());  
    })
};

const populateDb = async (db) => {
    await createTable(db);
    // console.debug("table created");
    await populateWithCsv(db);
    // console.debug("db populated");
}

const initDb = async () => {
    const db = createDb();
    await populateDb(db);

    // console.debug("db initialized");

    return db;
};

const run = () => {
    const args = process.argv.slice(2);
    args.length ? runAsCli() : runAsServer();
}

const main = async () => {
    db = await initDb();
    run();
};

main();