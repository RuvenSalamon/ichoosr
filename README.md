# install
`npm install`

Replace the accesstoken in index.html line 150 with one of your own. This token has since been revoked.  

# CLI
```
npm start -- --name Neud
```

# Web API
```
npm start
```

```
localhost:3000/cameras?camera=Neud
```

# Frontend
same server  
```
localhost:3000/
```
